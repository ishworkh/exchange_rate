# ExchangeRate

### Basic concepts:

Example for explanation: `EUR -> SEK = 8.9`
###### Base currency 
It is the currency we want to exchange money from and is a unit in exchange rates
mapping. For instance in example above `EUR` is base currency. And in this library
it is represented by ExchangeRate::BaseCurrency` class which has a knowledge about its
value in all available currencies (called counter currency)

###### Counter currency 
As briefly mentioned above, counter currency is the currency the exchange of currencies
happening to. For e.g. `SEK` is a counter currency.

###### Exchange Rate Clerk
It is an entity that has access to all base currencies, and can give exchange rate for
any available base_currency.

###### Service
It can be understood as a unique exchange rate provider. For eg. if we are using ecb data to provide
exchange rate in our system `ecb` could be a service, and hence it has its own implementation of
required objects (can also use default, if applicable for it).

###### ResourceResolver
It is an entity which answers the location for raw date required by a service.
Base class is `ExchangeRate::Counter::Raw::ResourceResolver`

###### ResoureParser
It is responsible for parsing resolved resource content to meaningful exchange rate values.
Base class is `ExchangeRate::Counter::Raw::ResourceParser`

###### ServiceRegister
It is a registry for available services. For a service to be usable, it has to be registered
in the registery with a unique name and service locator that provides its specific implementation of
different required components.

###### Config
Library config hash is accessible by `ExchangeRate.config`. Any config key can be added according to
need for a service, which can be used in service locator to make any type of component decisions.

 Global configs,
       
    config[:default_service], required as is used to choose the service in `ExchangeRate.at` method.
 
### Exchange Rate
 
This library is built taking extendibility into main consideration. Any exchange rate service can be extended
by two ways:

###### Register a new service

A new service can be extended any time by the user of the library. For which, one need to
create a service locator and register it via service register. For e.g,

    class MyNewServiceLocator
     include ExchangeRate::Service::BaseServiceLocator
    
     ........ implement required methods
    end

Must implement class is `ExchangeRate::Counter::Raw::ResourceParser`

For rests, default implementation could be used. However, that just depends on the requirement of each service.

Check `ExchangeRate::Service::BaseServiceLocator` or `ExchangeRate::Service::Ecb::ServiceLocator` to get an idea.

Then register it,
    
    ExchangeRate.add_service('my_service', MyNewServiceLocator) (recommended)
Or, 

    ExchangeRate::ServiceRegister.instance.register('my_service', MyNewServiceLocator)

###### Override components for existing service
For this method to work a service locator has to be implemented to allow override. If its not implemented that way, it will
throw an undefined method error.

For a service to allow this feature, its service locator should add an `attr_writer for the property it wants to
allow override. Like,

       class MyServiceLocator
         include ExchangeRate::Service::BaseServiceLocator
    
         attr_writer :counter_currency_cache
            
         # FOR OVERRIDES         
         def counter_currency_cache
            @counter_currency_cahce || MyCounterCurrencyCache.new
         end
       end

This will allow counter_currency_cache to be overridden.

And once its allowed, we can override simply by

    register = ExchangeRate::ServiceRegister.instance
    register.service_locator('my_service').counter_currency_cache=(new_currency_cache)

#### Ecb service
It uses ecb exchange rates data feed. 
Required configs,
    
    config[:service_name][:base_path], for file based resource
    config[:service_name][:reference_currency] , i.e. 'EUR'  

For ecb service to work, the raw `.xml` files should be downloaded to `config[:service_name][:base_path]/date`. 
For e.g, exchange rates for 2018/06/25 should be downloaded to `/tmp/ecb/2018_06_25` given `base_path` is `/tmp/ecb`

###### Note: 
Ecb service allows its cache to be overriden. So any implementation of `ExchangeRate::Counter::Cache` 
can be used to override current default `InMemory` cache, which only caches in memory, for optimization.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'exchange_rate', git: 'https://ishworkh@bitbucket.org/ishworkh/exchange_rate.git'
```

And then execute:

    $ bundle

## Usage
For ecd(default) service, 
    
    require 'exchange_rate'
    
    ExchangeRate.config {|c| c[:ecb][:base_path] = 'location/data/'}

    ExchangeRate.at(Date.today, 'EUR', 'USD')

And, if you want to use different service, (of course it has to be register service duh!!!) just
change the config value
    
    ExchangeRate.config { |c| c[:default_service] = 'my_awesome_service' }
    
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/exchange_rate.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
