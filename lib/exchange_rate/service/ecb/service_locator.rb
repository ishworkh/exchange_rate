module ExchangeRate
  module Service
    module Ecb
      # Service locator for ecb service
      class ServiceLocator
        include ExchangeRate::Service::BaseServiceLocator

        # Allow counter_currency_cache to be overridden in runtime
        attr_writer :counter_currency_cache

        protected

        def raw_resource_parser
          ExchangeRate::Service::Ecb::ResourceParser.new(raw_exchange_rate_factory)
        end

        def raw_resource_resolver
          ExchangeRate::Counter::Raw::File::FileResourceResolver.new(create_file_path_resolver, raw_resource_factory)
        end

        private

        def create_file_path_resolver
          ExchangeRate::Counter::Raw::File::PathResolver.new(ecb_base_path)
        end

        def ecb_base_path
          if ::ExchangeRate.config[:ecb][:base_path].nil?
            raise(StandardError, 'Please set a config value for ::ExchangeRate.config[:ecb][:base_path]')
          end
          ::ExchangeRate.config[:ecb][:base_path]
        end
      end
    end
  end
end
