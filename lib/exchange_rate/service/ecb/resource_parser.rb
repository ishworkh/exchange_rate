require 'nokogiri'
require 'date'

module ExchangeRate
  module Service
    module Ecb
      # Parses xml exchange rate feed that ecb provides.
      class ResourceParser < ExchangeRate::Counter::Raw::ResourceParser
        protected

        # Override
        def exchange_rates(content)
          xml_doc = ::Nokogiri::XML(content)
          date_nodes = xml_doc.xpath('//gesmes:Envelope/xmlns:Cube/xmlns:Cube')

          Enumerator.new do |yielder|
            date_nodes.each do |date_node|
              date = Date.parse(date_node.attr('time'))
              date_node.children.each do |child|
                if child.node_type == ::Nokogiri::XML::Node::ELEMENT_NODE
                  yielder << create_exchange_rate(date, 'EUR', child.attr('currency'), child.attr('rate').to_f)
                end
              end
            end
          end
        end
      end
    end
  end
end
