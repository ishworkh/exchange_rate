require 'singleton'

module ExchangeRate
  module Service
    # This module is supposed to be included in all service locators.
    # It has basic methods required for a service to do its job. And additionally, there
    # are some components which are very service specific, so needs to be implemented on per
    # service basis.
    # Things to note is that, this module exposes only three methods as public and rest are protected.
    # So any implementation that suffice the public interface can take a place as a service locator.
    # However, it is highly recommended to include this module for convinience
    module BaseServiceLocator
      def self.included(base)
        base.include Singleton
      end

      # Returns ExchangeRate::Counter::Raw::ExchangeRateReader
      # Value can be overriden in runtime, if the class implementing this module intend to do so.
      # To make it overridable, implementing class needs to add a writer for it, like
      #   attr_writer :raw_exchange_rate_reader
      def raw_exchange_rate_reader
        @raw_exchange_rate_reader || ExchangeRate::Counter::Raw::ExchangeRateReader.new(raw_resource_resolver)
      end

      # Returns ExchangeRate::Counter::InMemoryCache
      # Value can be overriden in runtime, if the class implementing this module intend to do so.
      # To make it overridable, implementing class needs to add a writer for it, like
      #   attr_writer :counter_currency_cache
      def counter_currency_cache
        @counter_currency_cache || ExchangeRate::Counter::InMemoryCache.new
      end

      # Returns ExchangeRate::Counter::CurrencyFactory
      # Value can be overriden in runtime, if the class implementing this module intend to do so.
      # To make it overridable, implementing class needs to add a writer for it, like
      #   attr_writer :counter_currency_factory
      def counter_currency_factory
        @counter_currency_factory || ExchangeRate::Counter::CurrencyFactory
      end

      protected

      # Returns service specific implementation of ExchangeRate::Counter::Raw::ResourceParser
      # Value can be overriden in runtime, if the class implementing this module intend to do so.
      # To make it overridable, implementing class needs to add a writer for it, like
      #   attr_writer :raw_resource_parser
      def raw_resource_parser
        @raw_resource_parser || raise(Errors::MethodNotImplementedError.create(__method__, self))
      end

      # Returns service specific implementation of ExchangeRate::Counter::Raw::ResourceResolver
      # Value can be overriden in runtime, if the class implementing this module intend to do so.
      # To make it overridable, implementing class needs to add a writer for it, like
      #   attr_writer :raw_resource_resolver
      def raw_resource_resolver
        @raw_resource_resolver || raise(Errors::MethodNotImplementedError.create(__method__, self))
      end

      # Returns ExchangeRate::Counter::Raw::ExchangeRateFactory
      # Value can be overriden in runtime, if the class implementing this module intend to do so.
      # To make it overridable, implementing class needs to add a writer for it, like
      #   attr_writer :raw_exchange_rate_factory
      def raw_exchange_rate_factory
        @raw_exchange_rate_factory || ExchangeRate::Counter::Raw::ExchangeRateFactory
      end

      # Returns ExchangeRate::Counter::Raw::ResourceFactory
      # Value can be overriden in runtime, if the class implementing this module intend to do so.
      # To make it overridable, implementing class needs to add a writer for it, like
      #   attr_writer :raw_resource_factory
      def raw_resource_factory
        @raw_resource_factory || ExchangeRate::Counter::Raw::ResourceFactory.new(raw_resource_parser)
      end
    end
  end
end
