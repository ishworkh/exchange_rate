module ExchangeRate
  # This class provides a interface to retrieve ExchangeRate::ExchangeRateClerkManager
  # for a registered service.
  class ServiceManager
    def initialize(service_register)
      @service_register = service_register
      @clerk_managers = {}
    end

    # Returns ExchangeRate::ExchangeRateClerkManager and cache in memory for further access
    # optimization.
    def exchange_rate_clerk_manager(service_name)
      cache_key = service_name.downcase

      unless @clerk_managers.has_key?(cache_key)
        service_locator = @service_register.service_locator(service_name)
        @clerk_managers[cache_key] = ExchangeRate::ExchangeRateClerkManager.new(
          counter_provider_manager(service_locator)
        )
      end

      @clerk_managers[cache_key]
    end

    private

    # Returns ExchangeRate::Counter::CurrencyProviderManager
    def counter_provider_manager(service_locator)
      ExchangeRate::Counter::CurrencyProviderManager.new(
        exchange_rate_reader(service_locator),
        currency_cache(service_locator),
        currency_factory(service_locator)
      )
    end

    # Returns service specific implementation of ExchangeRate::Counter::CurrencyFactory
    def currency_factory(service_locator)
      service_locator.counter_currency_factory
    end

    # Returns service specific implementation of ExchangeRate::Counter::Cache
    def currency_cache(service_locator)
      service_locator.counter_currency_cache
    end

    # Returns service specific implementation of ExchangeRate::Counter::Raw::ExchangeRateReader
    def exchange_rate_reader(service_locator)
      service_locator.raw_exchange_rate_reader
    end
  end
end
