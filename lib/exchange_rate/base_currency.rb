module ExchangeRate
  # This class represents a base_currency which has a knowledge of its value in all available counter
  # currencies.
  class BaseCurrency
    def initialize(counter_currencies)
      @counter_currencies = populate_counter_values(counter_currencies)
    end

    # Returns numeric value for self in given counter_currency.
    # Raises error if record for given counter currency is not found.
    def value_in(counter_currency)
      unless @counter_currencies.key? counter_currency
        raise CurrencyValueNotFoundError.create(counter_currency)
      end
      @counter_currencies[counter_currency]
    end

    private

    # Enumerates through counter currencies provided to generate hash of counter_currency => value pairs,
    # so that it is easily searchable.
    def populate_counter_values(counter_currencies)
      currencies = {}
      counter_currencies.each do |counter_currency|
        currencies[counter_currency.currency] = counter_currency.value
      end

      currencies
    end

    # Raises this error if a record for a counter currency is not found.
    class CurrencyValueNotFoundError < StandardError
      ERROR_MSG = 'Value for counter currency: %s not found'

      def self.create(counter_currency)
        new(sprintf(ERROR_MSG, counter_currency))
      end
    end
  end
end
