module ExchangeRate
  module Counter
    # This class is responsible for providing ExchangeRate::Counter::CurrencyProvider for a day.
    class CurrencyProviderManager
      def initialize(exchange_rate_reader, currency_cache, currency_factory)
        @exchange_rate_reader = exchange_rate_reader
        @currency_cache = currency_cache
        @currency_factory = currency_factory

        @providers = {}
      end

      # Returns an instance of ExchangeRate::Counter::CurrencyProvider for a given date.
      def currency_provider(day)
        cache_key = generate_key(day)
        @providers[cache_key] = create_provider(day) unless @providers.has_key?(cache_key)
        @providers[cache_key]
      end

      private

      def create_provider(day)
        ExchangeRate::Counter::CurrencyProvider.new(day, @exchange_rate_reader, @currency_cache, @currency_factory)
      end

      # Generate cache key for the day
      def generate_key(day)
        day.strftime('%Y-%m-%d')
      end
    end
  end
end
