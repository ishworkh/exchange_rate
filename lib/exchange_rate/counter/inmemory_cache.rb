module ExchangeRate
  module Counter
    # This class handles in-memory, volatile cache for counter currencies.
    class InMemoryCache
      include ExchangeRate::Counter::Cache

      def initialize
        @currencies = {}
      end

      protected

      # Override
      def get_cache_item(cache_key)
        @currencies[cache_key]
      end

      # Override
      def set_cache_item(cache_key, currencies)
        @currencies[cache_key] = currencies
      end

      # Override
      def cache_key_exist?(cache_key)
        @currencies.key? cache_key
      end
    end
  end
end
