module ExchangeRate
  module Counter
    # This class is an abstract cache interface for counter_currency objects.
    module Cache
      # Returns enumerator of ExchangeRate::Counter::Currency objects for a given cache key.
      # Checks beforehand if the cache_key exist, throws if not available.
      def get(cache_key)
        raise CachedCounterCurrenciesNotFoundError.create(cache_key) unless self.cache_key_exist? cache_key

        get_cache_item cache_key
      end

      # Saves enumerator of Counter::Currency objects to a given cache_key value.
      def set(cache_key, currencies)
        set_cache_item(cache_key, currencies)
      end

      protected

      # To be overridden by actual cache implementation.
      # Returns an enumerator of ExchangeRate::Counter::Currency objects.
      def get_cache_item(cache_key)
        raise Errors::MethodNotImplementedError.create(__method__, self)
      end

      # To be overridden by actual cache implementation.
      # Sets an enumerator of ExchangeRate::Counter::Currency objects to the given cache_key value.
      def set_cache_item(cache_key, currencies)
        raise Errors::MethodNotImplementedError.create(__method__, self)
      end

      # To be overridden by actual cache implementation.
      # Return true|false based on existence of cache_key.
      def cache_key_exist?(cache_key)
        raise Errors::MethodNotImplementedError.create(__method__, self)
      end

      # Raises this error if a key is not found in cache.
      class CachedCounterCurrenciesNotFoundError < StandardError
        ERROR_MSG = 'Cached currencies for key: %s not found'

        def self.create(key)
          new(sprintf(ERROR_MSG, key))
        end
      end
    end
  end
end
