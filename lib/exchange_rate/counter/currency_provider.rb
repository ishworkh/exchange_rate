module ExchangeRate
  module Counter
    # This class is responsible for providing counter currencies for a particular base currency
    # for a specific date.
    class CurrencyProvider
      def initialize(day, exchange_rate_reader, currency_cache, currency_factory)
        @day = day
        @exchange_rate_reader = exchange_rate_reader
        @currency_cache = currency_cache
        @currency_factory = currency_factory
      end

      # Expects base currency name as a param, and gives out an enumerator of ExchangeRate::Counter::Currency
      # objects. It tries to get values from cache if its cached already, otherwise provides them
      # from exchange_rate_reader after caching it.
      # Wraps a StandardError and raises CounterCurrencyProviderError if a StandardError is
      # caught during the process.
      def currencies(base_currency_code)
        begin
          cache_key = cache_key_for(base_currency_code)
          cache_if_not_exist(base_currency_code, cache_key)

          from_cache(cache_key)
        rescue => e
          raise CounterCurrencyProviderError.create(base_currency_code, e)
        end
      end

      private

      # Checks if counter currencies are available in cache, and cache them if not available.
      def cache_if_not_exist(currency_code, cache_key)
        begin
          from_cache(cache_key)
        rescue ExchangeRate::Counter::Cache::CachedCounterCurrenciesNotFoundError
          exchange_rates_for_day = @exchange_rate_reader.read(@day)
          counter_currencies = counter_currencies_for(exchange_rates_for_day, currency_code)
          @currency_cache.set(cache_key, counter_currencies)
        end
      end

      # Gets counter currencies from cache
      def from_cache(cache_key)
        @currency_cache.get(cache_key)
      end

      # Generate cache_key for a currency_code and date
      def cache_key_for(currency_code)
        sprintf '%s-%s', @day.strftime('%Y%m%d'), currency_code
      end

      # Filters counter currencies for a day out of all the exchange_rate objects exchange_rate_reader
      # provide and returns enumerator of mapping counter_currency object
      def counter_currencies_for(exchange_rates, base_currency_code)
        exchange_rates.map do |exchange_rate|
          if exchange_rate.base_currency_code == base_currency_code
            @currency_factory.create(exchange_rate.counter_currency_code, exchange_rate.value)
          else
            nil
          end
        end.compact
      end

      # Raises this exception if a StandardErorr is caught.
      class CounterCurrencyProviderError < StandardError
        ERROR_MSG = 'Error while trying to provide counter currencies for: %s, because: %s'

        def self.create(base_currency_code, prev)
          new(sprintf(ERROR_MSG, base_currency_code, prev.message))
        end
      end
    end
  end
end
