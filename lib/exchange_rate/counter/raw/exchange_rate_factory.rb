module ExchangeRate
  module Counter
    module Raw
      # Factory class responsible for creating ` ExchangeRate::Counter::Raw::ExchangeRate` object.
      class ExchangeRateFactory
        # Takes in strings base_currency_code, counter_currency_code and numeric value as params
        def self.create(date, base_currency_code, counter_currency_code, value)
          ::ExchangeRate::Counter::Raw::ExchangeRate.new(date, base_currency_code, counter_currency_code, value)
        end
      end
    end
  end
end
