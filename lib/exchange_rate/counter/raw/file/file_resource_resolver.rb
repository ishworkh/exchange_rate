module ExchangeRate
  module Counter
    module Raw
      module File
        # This class can be used to resolve local file based exchange rate raw resources.
        class FileResourceResolver < ::ExchangeRate::Counter::Raw::ResourceResolver
          def initialize(path_resolver, resource_factory, extension = nil)
            super(resource_factory)
            @path_resolver = path_resolver
            @extension = extension
          end

          protected

          # Override
          # Returns array of content of file resources found in correct path.
          # Raises error if path does not exist.
          # Filters file by extension if its given.
          # Maps filtered files to its content if the file is not a directory.
          # Raises not readable error, if file is not readable.
          def resource_contents(date)
            path = @path_resolver.resolve(date)
            check_path_exists(path)
            files_by_extension(path).map do |file|
              file_full_name = ::File.join(path, file)
              if ::File.directory?(file_full_name)
                nil
              else
                check_file_readable(file_full_name)
                ::File.read(file_full_name)
              end
            end.compact
          end

          private

          # Returns filtered enumerator of files in given path. Filters by extension is
          # it is not null otherwise returns all.
          def files_by_extension(path)
            files = ::Dir.foreach(path)
            files.select { |f| @extension.nil? || ::File.extname(f) == @extension }
          end

          def check_file_readable(full_path)
            raise FileNotReadableError.create(file_full_name) unless ::File.readable?(full_path)
          end

          # Check if a path exists in system.
          def check_path_exists(path)
            unless ::Dir.exist?(path)
              raise FileResourcePathDoesNotExistError.create(path)
            end
          end

          # Raises this if a file in not readable.
          class FileNotReadableError < StandardError
            ERROR_MSG = 'File: %s not readable'

            def self.create(file_name)
              new(sprintf(ERROR_MSG, file_name))
            end
          end

          # Raises this if given path does not exist.
          class FileResourcePathDoesNotExistError < StandardError
            ERROR_MSG = 'Path does not exist: %s'

            def self.create(path)
              new(sprintf(ERROR_MSG, path))
            end
          end
        end
      end
    end
  end
end
