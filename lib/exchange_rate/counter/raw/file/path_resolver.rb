module ExchangeRate
  module Counter
    module Raw
      module File
        class PathResolver
          def initialize(base_path)
            @base_path = base_path
          end

          def resolve(date)
            ::File.join(@base_path, date.strftime('%Y_%m_%d'))
          end
        end
      end
    end
  end
end
