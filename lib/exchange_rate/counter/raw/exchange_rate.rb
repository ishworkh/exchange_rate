module ExchangeRate
  module Counter
    module Raw
      # Object to represent exchange rate information received from raw source
      class ExchangeRate

        # date
        attr_reader :date

        # name of base currency
        attr_reader :base_currency_code

        # name of counter currency
        attr_reader :counter_currency_code

        # exchange rate value
        attr_reader :value

        def initialize(date, base_currency, counter_currency, value)
          @date = date
          @base_currency_code = base_currency
          @counter_currency_code = counter_currency
          @value = value
        end
      end
    end
  end
end
