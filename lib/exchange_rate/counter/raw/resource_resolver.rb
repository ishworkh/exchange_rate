module ExchangeRate
  module Counter
    module Raw
      # Responsible for finding resources providing exchange rate data for a
      # specific date. For e.g. file resources could be resolved for 2018-06-20 in
      # local path like #{base_path}/2018/06/20/, according to implementation of this
      # class.
      class ResourceResolver
        def initialize(resource_factory)
          @resource_factory = resource_factory
        end

        # Returns an enumerator of ExchangeRate::Counter::Raw::Resource
        def resolve(date)
          begin
            contents = resource_contents(date)
            raise NoResourceResolvedError.create unless contents.any?
            contents.map { |c| @resource_factory.create(c) }.each
          rescue => e
            raise ResourceResolverError.create(date, e)
          end
        end

        protected

        # Returns an array|enumerator of string content of resources
        def resource_contents(date)
          raise Errors::MethodNotImplementedError.create(__method__, self)
        end

        # Raises this error if a StandardError is caught.
        class ResourceResolverError < StandardError
          ERROR_MSG = 'Resource resolver failed for: %s because: %s'

          def self.create(date, prev)
            new(sprintf(ERROR_MSG, date.strftime('%Y-%m-%d'), prev.message))
          end
        end

        # Raises this error if no any resources were resolved
        class NoResourceResolvedError < StandardError
          ERROR_MSG = 'No resources were resolved'

          def self.create
            new(ERROR_MSG)
          end
        end
      end
    end
  end
end
