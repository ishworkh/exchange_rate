module ExchangeRate
  module Counter
    module Raw
      # Factory for resource class.
      class ResourceFactory
        # Expects mapping resource_parser for the resource type
        def initialize(resource_parser)
          @resource_parser = resource_parser
        end

        # Expects string content of resource
        def create(content)
          Resource.new(content, @resource_parser)
        end
      end
    end
  end
end
