module ExchangeRate
  module Counter
    module Raw
      # This class is responsible for parsing string content of resource and returning an enumerator of
      # ExchangeRate::Counter::Raw::ExchangeRate objects.
      class ResourceParser
        # Expects exchange_rate_factory as constructor parameter
        def initialize(exchange_rate_factory)
          @exchange_rate_factory = exchange_rate_factory
        end

        # Expect string content of a resource and returns enumerator of ExchangeRate::Counter::Raw::ExchangeRate
        # Wraps and raises ResourceParserFailedError if a StandardError is thrown during the process
        def parse(content)
          begin
            exchange_rates(content).each
          rescue => e
            raise ResourceParserFailedError.create(e)
          end
        end

        protected

        # Helper method to create ExchangeRate::Counter::Raw::ExchangeRate object
        def create_exchange_rate(date, base_currency, counter_currency, value)
          @exchange_rate_factory.create(date, base_currency, counter_currency, value)
        end

        # To be overridden by child classes implementing for a specific resource type
        # Expects a string content parameter and returns an enumerator of ExchangeRate::Counter::Raw::ExchangeRate
        def exchange_rates(content)
          raise Errors::MethodNotImplementedError.create(__method__, self)
        end

        # Raises this error if a StandardError is caught.
        class ResourceParserFailedError < StandardError
          ERROR_MSG = 'Resource parser failed because: %s'

          def self.create(prev)
            new(sprintf(ERROR_MSG, prev.message))
          end
        end
      end
    end
  end
end
