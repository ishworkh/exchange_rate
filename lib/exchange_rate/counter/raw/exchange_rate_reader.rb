module ExchangeRate
  module Counter
    module Raw
      # This class is responsible for providing raw exchange_rate objects for a specific date.
      # It accomplishes so by resolving resources with the help of resource_resolver for e.g. an
      # implementation of `ExchangeRate::Counter::Raw::ResourceResolver`.
      class ExchangeRateReader
        def initialize(resource_resolver)
          @resource_resolver = resource_resolver
        end

        # Public interface to this class
        # This will resolve resources, which in turn construct a enumerator with all exchange rate
        # object each resource provides for a given date.
        def read(date)
          begin
            resources = @resource_resolver.resolve(date)
            Enumerator.new do |y|
              resources.each do |resource|
                resource.exchange_rates.each do |exchange_rate|
                  y << exchange_rate if date === exchange_rate.date
                end
              end
            end
          rescue => e
            raise ExchangeRateReaderError.create(date, e)
          end
        end

        # Raise this error if a StandardError is caught in the process.
        class ExchangeRateReaderError < StandardError
          ERROR_MSG = 'Exchange rate reader failed for %s because: %s'

          def self.create(date, prev)
            new(sprintf(ERROR_MSG, date.strftime('%Y-%m-%d'), prev.message))
          end
        end
      end
    end
  end
end
