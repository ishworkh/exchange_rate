module ExchangeRate
  module Counter
    module Raw
      # Class to represent a single exchange rate resource. For e.g. a file containing an
      # XML with exchange rate values for a day is a resource.
      class Resource
        # Takes in two params, first one is string content of resource and second one is the
        # related parse for the type of content, for instance it could be a xml parser for a xml
        # resource type, json parser for json resource type
        def initialize(content, parser)
          @content = content
          @parser = parser
        end

        # Returns enumerator of exchange_rate object returned from mapping parser.
        def exchange_rates
          @parser.parse(@content).each
        end
      end
    end
  end
end
