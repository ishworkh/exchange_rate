module ExchangeRate
  module Counter
    # This class represents a currency
    class Currency
      # String id of currency i.e. EUR, SEK etc
      attr_reader :currency

      # Numeric value
      attr_reader :value

      def initialize(currency, value)
        @currency = currency
        @value = value
      end
    end
  end
end
