module ExchangeRate
  module Counter
    # This class creates ExchangeRate::Counter::Currency objects.
    class CurrencyFactory
      # Takes in two params. First, string id of currency and second number value of exchange rate.
      def self.create(currency_code, value)
        Currency.new(currency_code, value)
      end
    end
  end
end
