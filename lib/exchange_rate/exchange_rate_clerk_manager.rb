module ExchangeRate
  class ExchangeRateClerkManager
    # This class is responsible for providing ExchangeRate::ExchangeRateClerk for a day.
    def initialize(counter_provider_manager)
      @counter_provider_manager = counter_provider_manager
      @clerks = {}
    end

    # Returns ExchangeRate::ExchangeRateClerk for specific day(date)
    def exchange_rate_clerk(day)
      cache_key = get_cache_key(day)

      unless @clerks.has_key?(cache_key)
        base_currency_factory = ExchangeRate::BaseCurrencyFactory.new(day, @counter_provider_manager)
        @clerks[cache_key] = ExchangeRate::ExchangeRateClerk.new(base_currency_factory)
      end

      @clerks[cache_key]
    end

    private

    def get_cache_key(day)
      day.strftime('%Y-%m-%d')
    end
  end
end
