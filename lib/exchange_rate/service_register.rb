require 'singleton'

module ExchangeRate
  # This a singleton class responsible for tracking registered services.
  class ServiceRegister
    include Singleton

    def initialize
      @services = {}
    end

    # Adds a service and its locator to the register
    def register(service_name, service_locator)
      @services[service_name] = service_locator
    end

    # Returns implementation of ExchangeRate::Service::ServiceLocator for a registered service
    def service_locator(service_name)
      raise ServiceNotRegisteredError.create(service_name) unless @services.has_key?(service_name)

      @services[service_name]
    end

    # Raises this error if a service is not found in the registry.
    class ServiceNotRegisteredError < StandardError
      ERROR_MSG = 'Service: %s is not registered.'

      def self.create(service_name)
        new(sprintf(ERROR_MSG, service_name))
      end
    end
  end
end
