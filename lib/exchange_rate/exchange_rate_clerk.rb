module ExchangeRate
  # This class serves as an entity capable of providing exchange rate for available currency mapping
  class ExchangeRateClerk

    # Reference currency value, for e.g. for exchange rate feeds from ECB reference currency is 'EUR'
    attr_writer :reference_currency_code

    def initialize(base_currency_factory)
      @base_currency_factory = base_currency_factory
      @reference_currency_code = nil

      @base_currencies = {}
    end

    # Returns exchange rate value from `from` currency to `to` currency.
    # It tries to figure out exchange rate value from ExchangeRate::BaseCurrency object for `from` currency.
    # If it succeeds in doing that it gives out the value. However, if it does not succeed with that straight
    # strategy, it tries to deduce the value with a second strategy.
    # In second strategy, it gets value for both `from` and `to` currencies in relation to common reference currency
    # and calculate the value. For e.g.
    #                              if we have mappings,
    #                                 EUR => SEK = 8.9
    #                                 EUR => NOR = 10,
    #                              but not,
    #                                 SEK => NOR or NOR => SEK,
    # it can calculate value from SEK => NOR like,
    #                                 8.9 SEK = 10 NOR, from above mapping
    #                             then, 1 SEK = 10/8.9 NOR
    #                             or vice versa, 1 NOR = 8.9/10 SEK
    # However, for this strategy to work, a reference currency code has to be defined for the class, which is
    # basically a currency we know has mapping value to all required currencies. If it does not have a reference
    # currency defined, it will just raise an error coming from the first strategy it uses.
    def rate(from, to)
      begin
        begin
          from_base_currency = base_currency(from)
          from_base_currency.value_in(to)
        rescue ExchangeRate::BaseCurrency::CurrencyValueNotFoundError,
               ExchangeRate::BaseCurrencyFactory::CounterCurrenciesEmptyError => e
          raise e if @reference_currency_code.nil?

          reference_base_currency = base_currency(@reference_currency_code)
          value_in_from_currency = reference_base_currency.value_in(from)
          value_in_to_currency = reference_base_currency.value_in(to)

          value_in_to_currency / value_in_from_currency
        end
      rescue => e
        raise ExchangeRateClerkError.create(e)
      end
    end

    private

    # Creates and saves ExchangeRate::BaseCurrency object for a currency code.
    def base_currency(currency)
      @base_currencies[currency] = @base_currency_factory.create(currency) unless @base_currencies.has_key?(currency)
      @base_currencies[currency]
    end

    # Raises this error if it fails to provide exchange rate value.
    class ExchangeRateClerkError < StandardError
      ERROR_MSG = 'Exchange rate clerk failed to provide value because: %s'

      def self.create(prev)
        new(sprintf(ERROR_MSG, prev.message))
      end
    end
  end
end
