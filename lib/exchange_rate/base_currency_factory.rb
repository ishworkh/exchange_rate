module ExchangeRate
  # This class is responsible for creating ExchangeRate::BaseCurrency objects.
  class BaseCurrencyFactory
    # Requires date and ExchangeRate::Counter::CurrencyProviderManager as params
    def initialize(day, provider_manager)
      @day = day
      @provider_manager = provider_manager
    end

    # Returns ExchangeRate::BaseCurrency object for given currency code
    def create(currency)
      counter_currencies = @provider_manager.currency_provider(@day).currencies(currency)
      check_counter_currencies(currency, counter_currencies)

      ExchangeRate::BaseCurrency.new(counter_currencies)
    end

    private

    # Raises error if counter currencies provided is empty
    def check_counter_currencies(base_currency_code, currencies)
      raise CounterCurrenciesEmptyError.create(base_currency_code) unless currencies.any?
    end

    class CounterCurrenciesEmptyError < StandardError
      ERROR_MSG = 'Counter currencies not available(is empty) for base currency: %s'

      def self.create(base_currency_code)
        new(sprintf(ERROR_MSG, base_currency_code))
      end
    end
  end
end
