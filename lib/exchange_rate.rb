require "exchange_rate/version"

# exchange_rate/*
require 'exchange_rate/base_currency'
require 'exchange_rate/base_currency_factory'
require 'exchange_rate/exchange_rate_clerk'
require 'exchange_rate/exchange_rate_clerk_manager'
require 'exchange_rate/service_register'
require 'exchange_rate/service_manager'

# exchange_rate/counter/*
require 'exchange_rate/counter/currency'
require 'exchange_rate/counter/currency_factory'
require 'exchange_rate/counter/currency_provider'
require 'exchange_rate/counter/currency_provider_manager'
require 'exchange_rate/counter/cache'
require 'exchange_rate/counter/inmemory_cache'

# exchange_rate/counter/raw/*
require 'exchange_rate/counter/raw/exchange_rate'
require 'exchange_rate/counter/raw/exchange_rate_factory'
require 'exchange_rate/counter/raw/exchange_rate_reader'
require 'exchange_rate/counter/raw/resource'
require 'exchange_rate/counter/raw/resource_factory'
require 'exchange_rate/counter/raw/resource_parser'
require 'exchange_rate/counter/raw/resource_resolver'

# exchange_rate/counter/raw/file*
require 'exchange_rate/counter/raw/file/file_resource_resolver'
require 'exchange_rate/counter/raw/file/path_resolver'

# exchange_rate/service
require 'exchange_rate/service/base_service_locator'

# exchange_rate/service/ecb
require 'exchange_rate/service/ecb/service_locator'
require 'exchange_rate/service/ecb/resource_parser'

module ExchangeRate
  module Errors
    class MethodNotImplementedError < StandardError
      ERROR_MSG = 'Abstract method: %s not implemented in child class: %s'

      def self.create(method_name, klass)
        new(sprintf(ERROR_MSG, method_name, klass.class))
      end
    end
  end

  # Provide an easier interface to exchange rate system like,
  #   - ExchangeRate.at(day, from, to)
  #   - ExchangeRate.config
  #   - ExchangeRate.add_service
  #
  # `ExchangeRate.at` gives exchange rate value between any two available currencies
  # `ExchangeRate.config` allows to add configuration for the system.
  #   It can be configured like,
  #         ExchangeRate.config do {|c| c[:my_config] = 'testConfig'}
  #     and, can be accessed like,
  #         ExchangeRate.config[:my_config]
  # `ExchangeRate.add_service` can be used to add a service from any where in the project without having to
  #   extend|fork the library.
  class << self
    @@service_manager = ExchangeRate::ServiceManager.new(ExchangeRate::ServiceRegister.instance)
    @@configuration = {}

    # Main facade method to get exchange rate value
    # It by default uses a service that is set default in config i.e. config[:default_service]
    # Also, it sets reference_currency from config[service_name.to_sym][:reference_currency_code]
    def at(date, from, to)
      service_name = config[:default_service]
      clerk_manager = @@service_manager.exchange_rate_clerk_manager(service_name)

      clerk = clerk_manager.exchange_rate_clerk(date)
      clerk.reference_currency_code = config[service_name.to_sym][:reference_currency]

      clerk.rate(from, to)
    end

    # Lets get/set config depending on the block
    # This will be improved in later version, as this is a very primitive implementation for config.
    def config
      yield @@configuration if block_given?

      @@configuration
    end

    # Adds service to ExchangeRate::ServiceRegister
    # Additional thing it does is it also creates a placeholder config key
    #   For eg.
    #     add_service('my_service')
    #       will add a config, config[:my_service] = {}
    def add_service(service_name, service_locator_klass)
      ExchangeRate::ServiceRegister.instance.register(service_name, service_locator_klass.instance)
      config[service_name.to_sym] = {} unless config.has_key? service_name.to_sym
    end
  end


  # Lets add `ecb` service so it is available by default
  ExchangeRate.add_service('ecb', ExchangeRate::Service::Ecb::ServiceLocator)
  # Add `EUR` as reference currency for ecb service
  ExchangeRate.config[:ecb][:reference_currency] = 'EUR'

  # And also set it as default service
  ExchangeRate.config[:default_service] = 'ecb'
end




