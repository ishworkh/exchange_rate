require_relative 'spec_helper'

RSpec.describe ExchangeRate do
  it "has a version number" do
    expect(ExchangeRate::VERSION).not_to be nil
  end

  before do
    # reset counter currency cache
    ExchangeRate::Service::Ecb::ServiceLocator.instance.counter_currency_cache = nil
  end

  context 'ExchangeRate.config' do
    it 'saves configuration' do
      ExchangeRate.config do |c|
        c[:default_service] = 'something'
        c[:service_ecb] = { name: 'haila' }
      end

      expect(ExchangeRate.config[:default_service]).to eq('something')
      expect(ExchangeRate.config[:service_ecb][:name]).to eq('haila')
    end
  end

  context 'ExchangeRate.at' do
    it 'gives exchange rate value' do
      ExchangeRate.config do |c|
        c[:default_service] = 'ecb'
        c[:ecb][:base_path] = File.join(__dir__, 'fixture/ecb')
        c[:ecb][:reference_currency_code] = 'EUR'
      end
      expect(ExchangeRate.at(Date.new(2018, 6, 25), 'EUR', 'SEK')).to eq(10.3623)
      expect(ExchangeRate.at(Date.new(2018, 6, 25), 'NOK', 'SEK')).to eq(1.0941892020316146)
      expect(ExchangeRate.at(Date.new(2018, 6, 25), 'SEK', 'NOK')).to eq(0.9139187246074714)
    end
  end

  context 'ExchangeRate.add_service' do
    it 're adding servicing does not affect existing config' do
      ExchangeRate.config do |c|
        c[:default_service] = 'ecb'
        c[:ecb][:base_path] = File.join(__dir__, 'fixture/ecb')
        c[:ecb][:reference_currency_code] = 'EUR'
      end

      ExchangeRate.add_service('ecb', ExchangeRate::Service::Ecb::ServiceLocator)

      expect(ExchangeRate.at(Date.new(2018, 6, 25), 'EUR', 'SEK')).to eq(10.3623)
      expect(ExchangeRate.at(Date.new(2018, 6, 25), 'NOK', 'SEK')).to eq(1.0941892020316146)
      expect(ExchangeRate.at(Date.new(2018, 6, 25), 'SEK', 'NOK')).to eq(0.9139187246074714)
    end
  end
end
