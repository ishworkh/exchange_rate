require_relative '../spec_helper'

RSpec.describe ExchangeRate::ServiceManager do
  let(:service_name) { 'testservice' }
  let(:service_register) { double('test service register') }
  let(:service_locator) { double('service locator') }
  subject(:service_manager) { ExchangeRate::ServiceManager.new(service_register) }

  it 'gives exchange rate clerk manager for a service' do
    expect(service_locator).to receive(:counter_currency_factory).and_return(double)
    expect(service_locator).to receive(:counter_currency_cache).and_return(double)
    expect(service_locator).to receive(:raw_exchange_rate_reader).and_return(double)

    expect(service_register).to receive(:service_locator).with(service_name).and_return(service_locator)

    expect(service_manager.exchange_rate_clerk_manager(service_name)).to be_instance_of(ExchangeRate::ExchangeRateClerkManager)
  end

  it 'caches clerk manager for further access' do
    expect(service_locator).to receive(:counter_currency_factory).and_return(double)
    expect(service_locator).to receive(:counter_currency_cache).and_return(double)
    expect(service_locator).to receive(:raw_exchange_rate_reader).and_return(double)

    expect(service_register).to receive(:service_locator).with(service_name).and_return(service_locator)

    clerk_manager = service_manager.exchange_rate_clerk_manager(service_name)
    expect(service_manager.exchange_rate_clerk_manager(service_name)).to eq(clerk_manager)
  end
end
