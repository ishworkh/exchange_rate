require_relative '../../spec_helper'

RSpec.describe ExchangeRate::Service::BaseServiceLocator do
  it 'provides basic components for the service' do
    some_service_locator = Object.new
    some_service_locator.extend(ExchangeRate::Service::BaseServiceLocator)

    # because it depends on `raw_resource_resolver` which needs to be implemented service specific
    expect { some_service_locator.raw_exchange_rate_reader }.to raise_error(ExchangeRate::Errors::MethodNotImplementedError)
    expect(some_service_locator.counter_currency_cache).to be_instance_of(ExchangeRate::Counter::InMemoryCache)
    expect(some_service_locator.counter_currency_factory).to be(ExchangeRate::Counter::CurrencyFactory)
  end
end
