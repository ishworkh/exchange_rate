require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Service::Ecb::ResourceParser do
  let(:test_xml_multi_day) do
    <<-XML
<?xml version="1.0" encoding="UTF-8"?>
<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
  <gesmes:subject>Reference rates</gesmes:subject>
  <gesmes:Sender>
  <gesmes:name>European Central Bank</gesmes:name>
  </gesmes:Sender>
  <Cube>
    <Cube time="2018-06-22">
      <Cube currency="USD" rate="1.1648"/>
      <Cube currency="JPY" rate="128.3"/>
    </Cube>
    <Cube time="2018-06-21">
      <Cube currency="USD" rate="1.1538"/>
      <Cube currency="JPY" rate="127.59"/>
    </Cube>
  </Cube>
</gesmes:Envelope>
    XML
  end
  let(:test_xml_single_day) do
    <<-XML
<?xml version="1.0" encoding="UTF-8"?>
<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
  <gesmes:subject>Reference rates</gesmes:subject>
  <gesmes:Sender>
  <gesmes:name>European Central Bank</gesmes:name>
  </gesmes:Sender>
  <Cube>
    <Cube time="2018-06-22">
      <Cube currency="USD" rate="1.1648"/>
      <Cube currency="JPY" rate="128.3"/>
    </Cube>
  </Cube>
</gesmes:Envelope>
    XML
  end

  let(:exchange_rate_factory) { double('exchange rate factory') }

  subject(:parser) { ExchangeRate::Service::Ecb::ResourceParser.new(exchange_rate_factory) }

  it 'parses ecb exchange rates from multi day xml' do
    expect(exchange_rate_factory).to receive(:create) do |date, base_currency, counter_currency, value|
      expect(date === Date.new(2018, 6, 22)).to be true
      expect(base_currency).to eq('EUR')
      expect(counter_currency).to eq('USD')
      expect(value).to eq(1.1648)
    end
    expect(exchange_rate_factory).to receive(:create) do |date, base_currency, counter_currency, value|
      expect(date === Date.new(2018, 6, 22)).to be true
      expect(base_currency).to eq('EUR')
      expect(counter_currency).to eq('JPY')
      expect(value).to eq(128.3)
    end
    expect(exchange_rate_factory).to receive(:create) do |date, base_currency, counter_currency, value|
      expect(date === Date.new(2018, 6, 21)).to be true
      expect(base_currency).to eq('EUR')
      expect(counter_currency).to eq('USD')
      expect(value).to eq(1.1538)
    end
    expect(exchange_rate_factory).to receive(:create) do |date, base_currency, counter_currency, value|
      expect(date === Date.new(2018, 6, 21)).to be true
      expect(base_currency).to eq('EUR')
      expect(counter_currency).to eq('JPY')
      expect(value).to eq(127.59)
    end

    expect(parser.send(:exchange_rates, test_xml_multi_day).count).to eq(4)
  end

  it 'parses ecb exchange rates from single day xml' do
    expect(exchange_rate_factory).to receive(:create) do |date, base_currency, counter_currency, value|
      expect(date === Date.new(2018, 6, 22)).to be true
      expect(base_currency).to eq('EUR')
      expect(counter_currency).to eq('USD')
      expect(value).to eq(1.1648)
    end
    expect(exchange_rate_factory).to receive(:create) do |date, base_currency, counter_currency, value|
      expect(date === Date.new(2018, 6, 22)).to be true
      expect(base_currency).to eq('EUR')
      expect(counter_currency).to eq('JPY')
      expect(value).to eq(128.3)
    end

    expect(parser.send(:exchange_rates, test_xml_single_day).count).to eq(2)
  end
end
