require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Service::Ecb::ServiceLocator do
  subject(:locator) { ExchangeRate::Service::Ecb::ServiceLocator.instance }

  before do
    # Required by Ecb::ServiceLocator
    ExchangeRate.config { |c| c[:ecb][:base_path] = 'test/base/path' }
  end

  it 'provides own implementations of required objects' do
    expect(locator.raw_exchange_rate_reader).to be_instance_of(ExchangeRate::Counter::Raw::ExchangeRateReader)
    expect(locator.counter_currency_cache).to be_instance_of(ExchangeRate::Counter::InMemoryCache)
    expect(locator.counter_currency_factory).to be(ExchangeRate::Counter::CurrencyFactory)
  end

  it 'allows runtime overriding of currency cache' do
    new_cache = double('test currency cache')
    locator.counter_currency_cache = new_cache

    expect(locator.counter_currency_cache).to eq(new_cache)
  end
end
