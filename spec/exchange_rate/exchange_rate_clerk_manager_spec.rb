require_relative '../spec_helper'

RSpec.describe ExchangeRate::ExchangeRateClerkManager do
  let(:day) { Date.new }
  subject(:manager) { ExchangeRate::ExchangeRateClerkManager.new(double('counter provider manager')) }

  it 'gives exchange rate clerk' do
    expect(manager.exchange_rate_clerk(day)).to be_instance_of(ExchangeRate::ExchangeRateClerk)
  end

  it 'caches statically exchange_rate_clerk for a day once created' do
    clerk = manager.exchange_rate_clerk(day)
    expect(manager.exchange_rate_clerk(day)).to eq(clerk)
  end
end
