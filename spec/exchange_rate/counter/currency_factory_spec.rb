require_relative '../../spec_helper'

RSpec.describe ExchangeRate::Counter::CurrencyFactory do

  subject(:factory) { ExchangeRate::Counter::CurrencyFactory }

  it 'creates currency' do
    expect(factory.create('USD', 1.2)).to be_instance_of(ExchangeRate::Counter::Currency)
  end
end
