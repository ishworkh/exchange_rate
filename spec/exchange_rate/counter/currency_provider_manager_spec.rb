require_relative '../../spec_helper'
require 'date'

RSpec.describe ExchangeRate::Counter::CurrencyProviderManager do
  let(:day) { Date.new }
  let(:exchange_rate_reader) { double('exchange rate reader') }
  let(:currency_cache) { double('counter currency cache') }
  let(:currency_factory) { double('counter currency factory') }

  subject(:manager) { ExchangeRate::Counter::CurrencyProviderManager.new(exchange_rate_reader, currency_cache, currency_factory) }

  it 'gives currency provider for a day' do
    expect(manager.currency_provider(day)).to be_an_instance_of(ExchangeRate::Counter::CurrencyProvider)
  end

  it 'caches currency provider in memory' do
    currency_provider = manager.currency_provider(day)
    expect(manager.currency_provider(day)).to eq(currency_provider)
  end
end
