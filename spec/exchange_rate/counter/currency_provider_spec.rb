require_relative '../../spec_helper'

RSpec.describe ExchangeRate::Counter::CurrencyProvider do
  let(:date) { double('test date') }
  let(:formatted_date) { 'formatteddate' }
  let(:exchange_rate_reader) { double('exchange rate reader') }
  let(:currency_factory) { double('counter currency factory') }
  let(:currency_cache) { double('counter currency cache') }

  subject(:provider) { ExchangeRate::Counter::CurrencyProvider.new(date, exchange_rate_reader, currency_cache, currency_factory) }

  it 'wraps and raise error if something goes wrong' do
    expect(date).to receive(:strftime).with('%Y%m%d').and_return(formatted_date).once

    cache_key = "#{formatted_date}-EUR"

    expect(exchange_rate_reader).to receive(:read).with(date).and_raise(StandardError.new('test exception'))

    expect(currency_factory).to receive(:create).exactly(0).times

    expect(currency_cache).to receive(:get).with(cache_key).and_raise(ExchangeRate::Counter::Cache::CachedCounterCurrenciesNotFoundError)
    expect(currency_cache).to receive(:set).exactly(0).times

    expect {
      provider.currencies('EUR')
    }.to raise_error(ExchangeRate::Counter::CurrencyProvider::CounterCurrencyProviderError, /EUR.*test\sexception/)
  end

  context 'currencies not cached' do
    it 'provides counter currencies for base currency' do
      expect(date).to receive(:strftime).with('%Y%m%d').and_return(formatted_date).once

      cache_key = "#{formatted_date}-EUR"

      exchange_rate_eur1 = double('ExchangeRate',
                                  base_currency_code: 'EUR', counter_currency_code: 'USD', value: 1.3
      )
      exchange_rate_eur2 = double('ExchangeRate',
                                  base_currency_code: 'EUR', counter_currency_code: 'SEK', value: 8.9
      )
      exchange_rate_usd1 = double('ExchangeRate',
                                  base_currency_code: 'USD', counter_currency_code: 'EUR', value: 0.8
      )
      exchange_rate_usd2 = double('ExchangeRate',
                                  base_currency_code: 'USD', counter_currency_code: 'SEK', value: 5.6
      )

      enumerator = Enumerator.new do |yielder|
        yielder << exchange_rate_eur1
        yielder << exchange_rate_usd1
        yielder << exchange_rate_usd2
        yielder << exchange_rate_eur2
      end

      expect(exchange_rate_reader).to receive(:read).with(date).and_return(enumerator).once

      counter_currency1 = double
      counter_currency2 = double
      counter_currencies = [counter_currency1, counter_currency2]

      expect(currency_factory).to receive(:create).with('USD', 1.3).and_return(counter_currency1)
      expect(currency_factory).to receive(:create).with('SEK', 8.9).and_return(counter_currency2)

      expect(currency_cache).to receive(:get).with(cache_key).and_raise(ExchangeRate::Counter::Cache::CachedCounterCurrenciesNotFoundError)

      expect(currency_cache).to receive(:set).with(cache_key, counter_currencies)
      expect(currency_cache).to receive(:get).with(cache_key).and_return(counter_currencies)

      expect(provider.currencies('EUR')).to eq(counter_currencies)
    end
  end

  context 'currencies cached' do
    it 'provides counter currencies for base currency' do
      expect(date).to receive(:strftime).with('%Y%m%d').and_return(formatted_date).once

      cache_key = "#{formatted_date}-EUR"

      exchange_rate_reader = double
      expect(exchange_rate_reader).to receive(:read).exactly(0).times

      counter_currency1 = double
      counter_currency2 = double
      counter_currencies = [counter_currency1, counter_currency2]

      expect(currency_factory).to receive(:create).exactly(0).times

      expect(currency_cache).to receive(:get).with(cache_key).and_return(counter_currencies)
      expect(currency_cache).to receive(:get).with(cache_key).and_return(counter_currencies)
      expect(currency_cache).to receive(:set).exactly(0).times

      expect(provider.currencies('EUR')).to eq(counter_currencies)
    end
  end
end
