require_relative '../../spec_helper'

RSpec.describe ExchangeRate::Counter::InMemoryCache do
  let(:currencies) { [double('counter currency')] }

  subject(:cache) { ExchangeRate::Counter::InMemoryCache.new }

  it 'gets cached currencies' do
    key = 'key1'

    cache.set(key, currencies)
    expect(cache.get(key)).to eq(currencies)
  end

  it 'throws if cache does not exist' do
    expect { cache.get('random') }.to raise_error(ExchangeRate::Counter::Cache::CachedCounterCurrenciesNotFoundError)
  end
end
