require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Counter::Raw::ExchangeRate do
  let(:date) { Date.new }
  subject(:exchange_rate) { ExchangeRate::Counter::Raw::ExchangeRate.new(date, 'EUR', 'SEK', 8.9) }

  it 'gives raw exchange rate attributes' do
    expect(exchange_rate.date).to eq(date)
    expect(exchange_rate.base_currency_code).to eq('EUR')
    expect(exchange_rate.counter_currency_code).to eq('SEK')
    expect(exchange_rate.value).to eq(8.9)
  end
end
