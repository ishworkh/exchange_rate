require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Counter::Raw::ExchangeRateReader do
  let(:date) { Date.new }
  let(:resource_resolver) { double('resource resolver') }

  subject(:reader) { ExchangeRate::Counter::Raw::ExchangeRateReader.new(resource_resolver) }

  it 'gives out exchange rates for given date' do

    resource1 = double('resource')
    exchange_rate1 = double(ExchangeRate::Counter::Raw::ExchangeRate, date: date)
    exchange_rate2 = double(ExchangeRate::Counter::Raw::ExchangeRate, date: nil)
    resource1_exchange_rates = [exchange_rate1, exchange_rate2]
    expect(resource1).to receive(:exchange_rates).and_return(resource1_exchange_rates)

    resource2 = double('resource')
    exchange_rate3 = double(ExchangeRate::Counter::Raw::ExchangeRate, date: nil)
    exchange_rate4 = double(ExchangeRate::Counter::Raw::ExchangeRate, date: date)
    resource2_exchange_rates = [exchange_rate3, exchange_rate4]
    expect(resource2).to receive(:exchange_rates).and_return(resource2_exchange_rates)

    resource3 = double('resource')
    exchange_rate5 = double(ExchangeRate::Counter::Raw::ExchangeRate, date: nil)
    resource3_exchange_rates = [exchange_rate5]
    expect(resource3).to receive(:exchange_rates).and_return(resource3_exchange_rates)

    expect(resource_resolver).to receive(:resolve).with(date).and_return([resource1, resource3, resource2])

    expect(reader.read(date)).to match_array([exchange_rate1, exchange_rate4])
  end

  it 'raises error if resource resolver fails' do
    test_error = StandardError.new('testerror')

    expect(resource_resolver).to receive(:resolve).with(date).and_raise(test_error)

    expect { reader.read(date) }.to raise_error do |err|
      expect(err.message).to include('Exchange rate reader failed')
      expect(err.message).to include(date.strftime('%Y-%m-%d'))
      expect(err.message).to include('testerror')
    end
  end
end
