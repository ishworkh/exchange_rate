require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Counter::Raw::ResourceFactory do
  let(:resource_parser) { double('resource parser') }

  subject(:factory) { ExchangeRate::Counter::Raw::ResourceFactory.new(resource_parser) }

  it 'creates resource' do
    expect(factory.create('testcontent')).to be_instance_of(ExchangeRate::Counter::Raw::Resource)
  end
end
