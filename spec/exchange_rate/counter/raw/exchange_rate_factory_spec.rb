require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Counter::Raw::ExchangeRateFactory do
  subject(:factory) { ExchangeRate::Counter::Raw::ExchangeRateFactory }

  it 'creates exchange rate' do
    exchange_rate = factory.create(Date.new, 'EUR', 'SEK', 8.9)
    expect(exchange_rate).to be_instance_of(ExchangeRate::Counter::Raw::ExchangeRate)
  end
end
