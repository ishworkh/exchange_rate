require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Counter::Raw::ResourceParser do
  let(:resource_content) { 'here is the test content!!' }
  let(:exchange_rate_factory) { double('exchange rate factory') }
  subject(:parser) { ExchangeRate::Counter::Raw::ResourceParser.new(exchange_rate_factory) }

  context 'exchange_rates method no implemented' do
    it 'raises error' do
      expect { parser.parse('test content') }.to raise_error do |err|
        expect(err.message).to include 'not implemented'
      end
    end
  end

  context 'exchange_rate method implemented' do
    it 'gives exchange rates' do
      expected_exchange_rates = [double, double]

      allow(parser).to receive(:exchange_rates).with(resource_content).and_return(expected_exchange_rates)

      expect(parser.parse(resource_content).to_a).to eq(expected_exchange_rates)
    end

    it 'wraps error thrown from overriden method' do
      test_error = StandardError.new('test')
      allow(parser).to receive(:exchange_rates).with(resource_content).and_raise(test_error)

      expect { parser.parse(resource_content) }.to raise_error do |err|
        expect(err.message).to include 'test'
      end
    end
  end
end
