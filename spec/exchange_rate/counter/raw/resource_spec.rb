require_relative '../../../spec_helper'

RSpec.describe ExchangeRate::Counter::Raw::Resource do
  let(:content) { 'testcontents' }
  let(:parser) { double('resource parser') }

  subject(:resource) { ExchangeRate::Counter::Raw::Resource.new(content, parser) }

  it 'gives exchange rates' do
    expected_rates = [double, double]
    expect(parser).to receive(:parse).with(content).and_return(expected_rates)

    expect(resource.exchange_rates.to_a).to eq(expected_rates)
  end
end
