require_relative '../../../../spec_helper'
require 'date'

RSpec.describe ExchangeRate::Counter::Raw::File::PathResolver do
  let(:base_path) { '/tmp/base/path' }
  subject(:resolver) { ExchangeRate::Counter::Raw::File::PathResolver.new(base_path) }

  it 'resolves path for a date' do
    date = Date.new(2018, 6, 23)
    expect(subject.resolve(date)).to eq("#{base_path}/2018_06_23")
  end
end
