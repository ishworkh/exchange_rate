require_relative '../../../../spec_helper'
require 'tmpdir'
require 'date'

RSpec.describe ExchangeRate::Counter::Raw::File::FileResourceResolver do
  context 'resources exist' do
    before do
      @base_dir = File.join(Dir.tmpdir, 'test_service')
      Dir.mkdir(@base_dir)

      @date = Date.new(2018, 6, 23)
      @resource_base_path = File.join(@base_dir, @date.strftime('%Y_%m_%d'))
      Dir.mkdir(@resource_base_path)

      file1_name = 'abc.xml'
      @file1_content = 'this is test content1'
      @file1_path = File.join(@resource_base_path, file1_name)
      File.open(@file1_path, "w") do |f|
        f.write(@file1_content)
      end

      file2_name = 'cde.txt'
      @file2_content = 'this is test content2'
      @file2_path = File.join(@resource_base_path, file2_name)
      File.open(@file2_path, "w") do |f|
        f.write(@file2_content)
      end

      @to_be_skipped_dir = File.join(@resource_base_path, 'testdir.xml')
      Dir.mkdir(@to_be_skipped_dir)
    end

    after do
      File.unlink(@file1_path)
      File.unlink(@file2_path)
      Dir.unlink(@to_be_skipped_dir)
      Dir.unlink(@resource_base_path)
      Dir.unlink(@base_dir)
    end

    it 'returns all resources content' do
      path_resolver = double('file path resolver')
      expect(path_resolver).to receive(:resolve).with(@date).and_return(@resource_base_path)

      resource_factory = double('resource factory')

      resolver = ExchangeRate::Counter::Raw::File::FileResourceResolver.new(path_resolver, resource_factory)
      expect(resolver.send(:resource_contents, @date)).to match_array([@file1_content, @file2_content])
    end

    it 'returns filtered resources content if an extension filter given' do
      path_resolver = double('file path resolver')
      expect(path_resolver).to receive(:resolve).with(@date).and_return(@resource_base_path)

      resource_factory = double('resource factory')

      resolver = ExchangeRate::Counter::Raw::File::FileResourceResolver.new(path_resolver, resource_factory, '.xml')
      expect(resolver.send(:resource_contents, @date)).to match_array([@file1_content])
    end
  end

  context 'resources path does not exist' do
    before do
      @base_dir = File.join(Dir.tmpdir, 'test_service')
      Dir.mkdir(@base_dir)

      @date = Date.new(2018, 6, 23)
      @resource_base_path = File.join(@base_dir, @date.strftime('%Y_%m_%d'))
    end

    after do
      Dir.unlink(@base_dir)
    end

    it 'raises error' do
      path_resolver = double('file path resolver')
      expect(path_resolver).to receive(:resolve).with(@date).and_return(@resource_base_path)

      resource_factory = double('resource factory')

      resolver = ExchangeRate::Counter::Raw::File::FileResourceResolver.new(path_resolver, resource_factory)
      expect { resolver.send(:resource_contents, @date) }.to raise_error(
                                                               ExchangeRate::Counter::Raw::File::FileResourceResolver::FileResourcePathDoesNotExistError,
                                                               /#{@resource_base_path}/)
    end
  end
end
