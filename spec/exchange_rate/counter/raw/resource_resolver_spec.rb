require_relative '../../../spec_helper'
require 'date'

RSpec.describe ExchangeRate::Counter::Raw::ResourceResolver do
  let(:date) { Date.new }
  let(:resource_factory) { double('resource factory') }

  subject(:resolver) { ExchangeRate::Counter::Raw::ResourceResolver.new(resource_factory) }

  context 'method "resources_for" not overriden' do
    it 'raises error' do
      expect { resolver.resolve(date) }.to raise_error do |err|
        expect(err.message).to include 'not implemented'
      end
    end
  end

  context 'method "resources_for" overriden' do
    it 'returns enumerator of resource' do
      expected_resources_content = ['first resource', 'second resource']
      expected_resources = [double, double]

      expect(resource_factory).to receive(:create).with(expected_resources_content[0]).and_return(expected_resources[0])
      expect(resource_factory).to receive(:create).with(expected_resources_content[1]).and_return(expected_resources[1])

      expect(resolver).to receive(:resource_contents).with(date).and_return(expected_resources_content)

      expect(resolver.resolve(date).to_a).to eq(expected_resources)
    end

    it 'raises error if no resources were resolved' do
      expected_resources_content = []

      expect(resolver).to receive(:resource_contents).with(date).and_return(expected_resources_content)

      expect { resolver.resolve(date) }.to raise_error(ExchangeRate::Counter::Raw::ResourceResolver::ResourceResolverError,
                                                       /No resources were resolved/)
    end
  end
end
