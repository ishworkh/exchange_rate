require_relative '../../spec_helper'

RSpec.describe ExchangeRate::Counter::Currency do
  subject(:counter_currency) { ExchangeRate::Counter::Currency.new('EU', 1.2) }

  it 'gives currency and value' do
    expect('EU').to eq(counter_currency.currency)
    expect(1.2).to eq(counter_currency.value)
  end
end
