require_relative '../spec_helper'

RSpec.describe ExchangeRate::ExchangeRateClerk do
  let(:from_base_currency) { double('base currency') }
  let(:reference_base_currency) { double('reference base currency') }
  let(:base_currency_factory) { double('base currency factory') }

  subject(:clerk) { ExchangeRate::ExchangeRateClerk.new(base_currency_factory) }

  context 'without reference currency' do
    it 'gives exchange rate' do
      to_currency_code = 'NOR'
      to_currency_rate = 9.8

      from_currency_code = 'EUR'
      expect(from_base_currency).to receive(:value_in).with(to_currency_code).and_return(to_currency_rate)
      expect(base_currency_factory).to receive(:create).with(from_currency_code).and_return(from_base_currency)

      expect(clerk.rate(from_currency_code, to_currency_code)).to eq(to_currency_rate)
    end

    it 'throws when exchange rate not found' do
      to_currency_code = 'NOR'

      from_currency_code = 'EUR'
      expect(from_base_currency).to receive(:value_in).with(to_currency_code).and_raise(StandardError.new('testerror'))
      expect(base_currency_factory).to receive(:create).with(from_currency_code).and_return(from_base_currency)

      expect { clerk.rate(from_currency_code, to_currency_code) }.to raise_error(ExchangeRate::ExchangeRateClerk::ExchangeRateClerkError, /testerror/)
    end
  end

  context 'with reference currency available' do
    context 'base currency has a direct mapping' do
      it 'gives exchange rate' do
        to_currency_code = 'NOR'
        to_currency_rate = 0.8

        from_currency_code = 'SEK'
        expect(from_base_currency).to receive(:value_in).with(to_currency_code).and_return(to_currency_rate)
        expect(base_currency_factory).to receive(:create).with(from_currency_code).and_return(from_base_currency)

        clerk.reference_currency_code = 'EUR'
        expect(clerk.rate(from_currency_code, to_currency_code)).to eq(to_currency_rate)
      end
    end

    context 'base currency has no direct mapping' do
      it 'gives exchange rate' do
        # Test mappings:
        #   SEK - NOR = 0.92
        #   EUR - NOR = 9.45
        #   EUR - SEK = 10.33

        from_currency_code = 'SEK'
        to_currency_code = 'NOR'

        test_error = ExchangeRate::BaseCurrency::CurrencyValueNotFoundError.new('testerror')
        expect(from_base_currency).to receive(:value_in).with(to_currency_code).and_raise(test_error)

        reference_currency_code = 'EUR'
        expect(reference_base_currency).to receive(:value_in).with(from_currency_code).and_return(10.33)
        expect(reference_base_currency).to receive(:value_in).with(to_currency_code).and_return(9.45)

        expect(base_currency_factory).to receive(:create).with(from_currency_code).and_return(from_base_currency)
        expect(base_currency_factory).to receive(:create).with(reference_currency_code).and_return(reference_base_currency)

        clerk.reference_currency_code = 'EUR'
        expect(clerk.rate(from_currency_code, to_currency_code)).to eq(9.45 / 10.33)
      end
    end

    it 'throws if secondary strategy fails' do
      # Test mappings:
      #   SEK - NOR = 0.92
      #   EUR - NOR = 9.45
      #   EUR - SEK = 10.33

      from_currency_code = 'SEK'
      to_currency_code = 'NOR'

      test_error1 = ExchangeRate::BaseCurrency::CurrencyValueNotFoundError.new('testerror1')
      expect(from_base_currency).to receive(:value_in).with(to_currency_code).and_raise(test_error1)

      reference_currency_code = 'EUR'
      expect(reference_base_currency).to receive(:value_in).with(from_currency_code).and_return(10.33)

      test_error2 = ExchangeRate::BaseCurrency::CurrencyValueNotFoundError.new('testerror2')
      expect(reference_base_currency).to receive(:value_in).with(to_currency_code).and_raise(test_error2)

      expect(base_currency_factory).to receive(:create).with(from_currency_code).and_return(from_base_currency)
      expect(base_currency_factory).to receive(:create).with(reference_currency_code).and_return(reference_base_currency)

      clerk.reference_currency_code = 'EUR'
      expect { clerk.rate(from_currency_code, to_currency_code) }.to raise_error(ExchangeRate::ExchangeRateClerk::ExchangeRateClerkError,
                                                                                 /testerror2/)
    end
  end
end
