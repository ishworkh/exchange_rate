require_relative '../spec_helper'

RSpec.describe ExchangeRate::BaseCurrency do
  subject do
    counter_currency_value1 = double('value1', currency: 'EUR', value: 0.67)
    counter_currency_value2 = double('value2', currency: 'USD', value: 0.9)

    base_currency_values = double
    allow(base_currency_values).to receive(:each) do |&block|
      block.call(counter_currency_value1)
      block.call(counter_currency_value2)
    end

    ExchangeRate::BaseCurrency.new(base_currency_values)
  end

  it 'gives value for asked counter currency' do
    expect(subject.value_in('USD')).to eq(0.9)
    expect(subject.value_in('EUR')).to eq(0.67)
  end

  it 'throws if value for counter currency is not found' do
    expect { subject.value_in('random') }.to raise_error(
                                               ExchangeRate::BaseCurrency::CurrencyValueNotFoundError,
                                               /random/
                                             )
  end
end
