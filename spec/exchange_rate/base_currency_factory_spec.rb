require_relative '../spec_helper'

RSpec.describe ExchangeRate::BaseCurrencyFactory do
  let(:currency_code) { 'USD' }
  let(:day) { Date.new }

  it 'creates BaseCurrency' do
    counter_currency1 = double(ExchangeRate::Counter::Currency, currency: 'EUR', value: 0.8)
    counter_currency2 = double(ExchangeRate::Counter::Currency, currency: 'SEK', value: 6.8)

    counter_currency_provider = double('currency provider')
    expect(counter_currency_provider).to receive(:currencies).with(currency_code).and_return(
      [counter_currency1, counter_currency2]
    )

    provider_manager = double('currency provider manager')
    expect(provider_manager).to receive(:currency_provider).with(day).and_return(counter_currency_provider)

    factory = ExchangeRate::BaseCurrencyFactory.new(day, provider_manager)
    expect(factory.create(currency_code)).to be_instance_of(ExchangeRate::BaseCurrency)
  end

  it 'raises error if counter currencies is empty' do
    counter_currency_provider = double('currency provider')
    expect(counter_currency_provider).to receive(:currencies).with(currency_code).and_return([])

    provider_manager = double('currency provider manager')
    expect(provider_manager).to receive(:currency_provider).with(day).and_return(counter_currency_provider)

    factory = ExchangeRate::BaseCurrencyFactory.new(day, provider_manager)

    expect { factory.create(currency_code) }.to raise_error(ExchangeRate::BaseCurrencyFactory::CounterCurrenciesEmptyError,
                                                            /is empty/)
  end
end
