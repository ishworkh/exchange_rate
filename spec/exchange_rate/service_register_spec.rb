require_relative '../spec_helper'

RSpec.describe ExchangeRate::ServiceRegister do
  subject(:service_register) { ExchangeRate::ServiceRegister.instance }

  it 'registers a service' do
    service_name = 'test_service'
    service_locator = double('test service locator')

    service_register.register(service_name, service_locator)
    expect(service_register.service_locator(service_name)).to eq(service_locator)
  end

  it 'raises error if service is not found' do
    expect { service_register.service_locator('randomservice') }.to raise_error(ExchangeRate::ServiceRegister::ServiceNotRegisteredError,
                                                                                /randomservice/)
  end

  it 'is singleton' do
    expect { ExchangeRate::ServiceRegister.new }.to raise_error(StandardError)
    expect(ExchangeRate::ServiceRegister.instance).to eq(ExchangeRate::ServiceRegister.instance)
  end
end
